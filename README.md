# Domain Adaptation Challenge

[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1SgI7f2crr8-PG7GkAExCg4onUXV-A3zJ?usp=sharing)



Project aims to train the amazon dataset and test on webcam and try to achieve maximum accuracy. Fourier Domain Adaptation (FDA) can be used for better accuracy. Lastly this repos helps in comparing results obtained by using FDA, without using FDA and other domain adaptation (DA) methods. 

---


## Codes and Directories
Explaination of codes written and its use cases

*To be added: How to use the code*


### [CAN_net.ipynb](CAN_net.ipynb)
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/14sW16o8DHDwcNWhfqeifOrfet-AIMWS_?usp=sharing)
- Implemented "Contrastive Adaptation Network for Unsupervised Domain Adaptation task"
- Trained for two kinds of dataset
  - amazon transformed to webcam images
  - unaltered amazon dataset images
- Official code is opensourced ([Link](https://github.com/kgl-prml/Contrastive))

### [dataset_fft_analysis.ipynb](dataset_fft_analysis.ipynb)
- Analyse images in Fourier domain using OpenCV
- Do low pass filtering and observe 
- Do high pass filtering and observe 
- Merge low pass filtered image of source with high pass filterd images of target

### [FDA.ipynb](FDA.ipynb)
- Official paper code of FDA ([Link](https://github.com/YanchaoYang/FDA)). 
- To be used for dataset generation and transformation of amazon images to webcam images
- Cant be used easily for visualization of FFT transformation
- Use custom written code for analysis [dataset_fft_analysis.ipynb](dataset_fft_analysis.ipynb) 
### [resnet18_pretrained_infer.ipynb](resnet18_pretrained_infer.ipynb) 
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1SgI7f2crr8-PG7GkAExCg4onUXV-A3zJ?usp=sharing)
- Used for infereing results with pre-trained network
- PyTorch based code
- ResNet18 architecture used
- Provided good accuracy, but many classes were missing in ImageNet dataset

### [training_classifier.ipynb](training_classifier.ipynb)
- Setting up custom classifier training
- Used transfer learning and changed last layer dimension to `(in_features, num_classes)`
- ResNet18 architecutre used 
  - Traing with FDA transforemd images 
  - Training with vanilla amazon images 

### [results](results)
- Consists training logs obtained when training CAN for [amazon to webcam](results/log_a2w.txt) and [amazonFDA to webcam](results/log_f2w.txt)
- Consists of Fourier analysis of images of good and bad results of FDA

## Dataset

- [domain_adaptation_images](domain_adaptation_images)
  - Consists of offical `Office31` dataset
  - Used `amazon` and `webcam` dataset
  - `amazon` was treated as source and `webcam` as target
- [FDA_trans_dataset](FDA_trans_dataset)
  - FDA done to minimise gap between `amazon` and `webcam`
  - Acted as training set

## Supporting links
These are personal links and resources which are used for better understanding of the topic.
- Presentation - [Link](https://docs.google.com/presentation/d/1pcOrpnPmfg_6nQQQ2R_rcaFhshcOg8btdRlzy7_oibs/edit?usp=sharing)
- HackMD notes on FDA - [Link](https://hackmd.io/@take2rohit/BkE3-GUVd)
- Lecure Notes on GoodNotes5 iCloud 


## References
- [Office Dataset](https://people.eecs.berkeley.edu/~jhoffman/domainadapt/)
- [Fourier Domain Adaptation for Semantic Segmentation](https://openaccess.thecvf.com/content_CVPR_2020/papers/Yang_FDA_Fourier_Domain_Adaptation_for_Semantic_Segmentation_CVPR_2020_paper.pdf)
- [Contrastive Adaptation Network for Unsupervised Domain Adaptation paper (CVPR 2019)](https://github.com/kgl-prml/Contrastive-Adaptation-Network-for-Unsupervised-Domain-Adaptation)
- [Contrastive Adaptation Network Github](https://github.com/kgl-prml/Contrastive-Adaptation-Network-for-Unsupervised-Domain-Adaptation)
- [YouTube Video Lecture (17,18)](https://youtu.be/El760ZzsXN8)
- [A Survey of Unsupervised Deep Domain Adaptation ](https://arxiv.org/pdf/1812.02849.pdf)


## Contributor
- **Rohit Lal** - [Website](http://take2rohit.github.io/)